#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMovie>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QMovie *lol = new QMovie(":/GIF/cody-commander-cody.gif");

    ui->codyGIF->setMovie(lol);
    lol->start();

    QMovie *lol2 = new QMovie(":/GIF/brandon-lee-the.gif");

    ui->crowGIF->setMovie(lol2);
    lol2->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_actionWybierz_GIF1_triggered()
{
    QFileDialog select(this);
    select.setNameFilter("Pliki GIF (*.gif)");
    QString gif1_new = select.getOpenFileName(this);

    QMovie *lol = new QMovie(gif1_new);

    ui->codyGIF->setMovie(lol);
    lol->start();
}

